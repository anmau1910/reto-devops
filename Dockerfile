FROM node:14-alpine3.14 as build

WORKDIR /app

COPY . ./

RUN npm install

FROM gcr.io/distroless/nodejs

COPY --from=build /app /

USER 5000

EXPOSE 3000

CMD ["index.js"]
